function [y]=phibase(x,i,a,b)

if (i==1)
	y=(1/(b-a))*x-a/(b-a);%croissant
elseif (i==2)
    y=(1/(a-b))*x-b/(a-b);%décroissant
else
	fprintf("error sur i\n")
end

% pour l'affichage
j=0;
X=[];
Y=[];
while (j<=100)
    x=j*((b-a)/100) +a;
    X=[X,x];
    if (i==2)
        Y=[Y,(1/(a-b)).*x-b/(a-b)];
    else
        Y=[Y,(1/(b-a)).*x-a/(a-b)];
    end
    j=j+1;
end
plot(X,Y)


