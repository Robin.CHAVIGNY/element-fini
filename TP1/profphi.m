function [y]=profphi(x,f,i,a,b)
phi=phibase(x,i,a,b);
y0=feval(f,x);
y=y0*phi;