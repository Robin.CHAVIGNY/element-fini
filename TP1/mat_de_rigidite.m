function [R]=mat_de_rigidite(a,b,n)

R=zeros(n+1);

h=(b-a)/n;
x=[a:h:b];

for k = 1:n
    for iloc = 1:2
        iglob = iloc + k-1;
        for jloc = 1:2
            jglob = jloc + k-1;
            R(iglob,jglob) = R(iglob,jglob) + simpson(@proprimphi,x(k),x(k+1),x(k),x(k+1),iloc,jloc);
        end
    end
end


% for i= 1:n
%     for j = 1:n
%         if (i==j-1)
%             R(i,j)=feval(fun,@proprimphi,x(j),x(j+1),x(j),x(j+1),0);
%         elseif (i==j+1)
%             R(i,j)=feval(fun,@proprimphi,x(i),x(i+1),x(i),x(i+1),0);
%         elseif(i==j)
%             if(i==1)
%                 R(i,j)=feval(fun,@proprimphi,x(i),x(i+1),x(i),x(i+1),2);
%             elseif(i==n)
%                 R(i,j)=feval(fun,@proprimphi,x(i),x(i+1),x(i),x(i+1),1);
%             else
%                 R(i,j)=feval(fun,@proprimphi,x(i),x(i+1),x(i),x(i+1),1)+feval(fun,@proprimphi,x(i+1),x(i+2),x(i+1),x(i+2),2);
%             end
%         end
%     end
% end