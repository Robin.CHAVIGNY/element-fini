function [c]=main(fun,a,b,n,type,u1,u2)
%% main : résolution de problème de Neumann ou Dirichlet
%
% Synopsis:
%       Résoudre un problème du types -beta*u"+alpha*u=f avec des
%       conditions au limites (sur u'(0) et u'(1) pour Neumann et
%       sur u(0) et u(1) pour Dirichlet).
%
% Inputs:
%       fun     : fonction dont on veut la résolution 
%       a       : born inférieur de l'interval de résolution
%       b       : born supérieur de l'interval de résolution
%       n       : nombre de sous intervals
%       type    : entier qui vaut 0 pour un problème de Neumann
%                 et 1 pour un problème de Dirichlet
%       u1,u2   : valeur des condition au limites suivant le type 
%                 du problème
% Outputs:
%       c       : point pour chaque sous interval représentant la 
%                 fonction u solution du problème
% 
%-------------------------------------------------------------------

if (type==0)
    upa=u1;upb=u2;
elseif (type==1)
    ua=u1;ub=u2;
else
    print("erreur de type")
end


alpha=1;beta=1;

M=mat_de_masse(a,b,n);
R=mat_de_rigidite(a,b,n);
B=vec_b(a,b,n,fun);



A= alpha*M + beta*R;%alpha =1 et beta =1

% Neumann
if (type==0)
    A(1,1) = 10^7; A(n+1,n+1) = 10^7;
    B(1) = B(1)-upa; B(n+1) = B(n+1)+upb;
end

c=A\B;

% Dirichlet
if(type==1)
    Anew = A([2:n],[2:n]);
    Bnew = B - ua*A(:,1) - ub*A(:,n+1);
    Bnew = Bnew([2:n]);
    
    % Calcul de la solution
    c(1) = ua;
    c([2:n]) = Anew\Bnew;
    c(n+1) = ub;
end


x = linspace(a, b, n+1);


plot(x,c,'red')
axis equal

            