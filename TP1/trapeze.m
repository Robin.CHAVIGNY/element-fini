function [y]=trapeze(f,a,b,varargin)

fa=feval(f,a,varargin{:});
fb=feval(f,b,varargin{:});
y=(b-a)*(fa+fb)/2;

end
