beta = 1;
alpha = 1;

aa = 0;
bb = 1;

n = 10;
h = (bb-aa)/n; % nombre d'intervalles et pas uniforme de chaque intervalle

xm = aa:h:bb;

% Assemblage du membre de droite :
F = sparse(n+1,1); % initialisation
for k = 1:n % Considération du k-ieme intervalle [xm(k),xm(k+1)] :
    for il = 1:2 % il = ilocal
        ig = k-1 + il; % ig = iglobal
        F(ig) = F(ig) + integsimpson('phibasef',xm(k) ,xm(k+1),il,k,xm);
    end
end


% Assemblage matrices :
K = sparse(n+1,n+1); % initialisation
M = K; % initialisation
for k = 1:n % Considération du k-ieme intervalle [xm(k),xm(k+1)] :
    for il =1:2 % il = ilocal
        ig = k-1 + il; % ig = iglobal
        for jl = 1:2 % jl = jlocal
            jg = k-1 + jl; % jg = jglobal
            M(ig,jg) = M(ig,jg) + integsimpson('phiphibase',xm(k),xm(k+1),il,jl,k,xm);
            K(ig,jg) = K(ig,jg) + integsimpson('phiderphiderbase',xm(k),xm(k+1),il,jl,k,xm);
        end
    end
end
full(K)
K = beta*K + alpha*M; % Matrice du membre de gauche :

% Prise en compte des conditions de Dirichlet homogène, par pénalisation :
K(1,1) = 10^7; K(n+1,n+1) = 10^7;
F(1) = 0; F(n+1) = 0;% plus généralement F(1) = u(aa) et F(n+1) = u(bb) données du problème

% Calcul de la solution :
u = K\F % résolution de K*u = F par la méthode LU.
%full(M)
%full(F)
plot(xm,u) % Dessin du graphe de la solution u(x) :
axis equal



function y = phibase1(x,il,k,xm)
% Pour il=1,2, les 2 fonctions phi_{il} de base P1 sur le kième intervalle [xm(k),xm(k+1)]
% (les deux demi-chapeaux). Ici il = numéro local à l'intervalle.
if (il==1)
    y = (x-xm(k+1)) ./ (xm(k)-xm(k+1)); % décroissante sur [xm(k),xm(k+1)]
else
    y = (x-xm(k)) ./ (xm(k+1)-xm(k)); % croissante sur [xm(k),xm(k+1)]
end
end


function y = phiphibase(x,il,jl,k,xm)
y = phibase1(x,il,k,xm) .* phibase1(x,jl,k,xm);
end

function y = phiderbase(x,il,k,xm)
if (il==1)
    y = -ones(size(x)) / (xm(k+1)-xm(k)); % ou bien pdbaseab = -x.^0
else
    y = ones(size(x)) / (xm(k+1)-xm(k));
end
end

function y = phiderphiderbase(x,il,jl,k,xm)
% produit phi_{il}'*phi_{jl}' des dérivées des fonctions de base, sur le k-ième intervalle
y = phiderbase(x,il,k,xm).*phiderbase(x,jl,k,xm);
end

function y = phibasef(x,il,k,xm)
% produit phi_{il}(x)*f(x) sur l'intervalle k, sur le k-ième intervalle
y = phibase1(x,il,k, xm).*fb(x);
end

function y = fb(x)
% fonction pour le membre de droite
%y = 10*x.^0;
%y=ones(size(x));
y=10*x;
end

function val = integsimpson(fun,a,b,varargin)
% intégration de fonction fun par simpson sur [a,b]
c = (a+b) / 2;
y = feval(fun,[a c b],varargin{:});
fa = y(1);
fc = y(2);
fb = y(3);
val = (b-a) * (fa+4*fc+fb) / 6;
end

