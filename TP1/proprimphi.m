function [y]=proprimphi(x,a,b,iloc,jloc)


phii=primphibase(x,iloc,a,b);
phij=primphibase(x,jloc,a,b);

y=phii*phij;