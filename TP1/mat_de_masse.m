function [M]=mat_de_masse(a,b,n)

%M=zeros(n+1);
M=sparse(n+1,n+1);

h=(b-a)/n;
x=[a:h:b];

for k = 1:n % Considération du k-ieme intervalle [xm(k),xm(k+1)] :
    for il =1:2 % il = ilocal
        ig = k-1 + il;% ig = iglobal
        for jl = 1:2 % jl = jlocal
            jg = k-1 + jl; % jg = jglobal
            M(ig,jg) = M(ig,jg) + simpson(@prophi,x(k),x(k+1),x(k),x(k+1),il,jl);
        end
    end
end