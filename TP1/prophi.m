function [y]=prophi(x,a,b,i1,i2)

phii=phibase(x,i1,a,b);
phij=phibase(x,i2,a,b);

y=phii*phij;