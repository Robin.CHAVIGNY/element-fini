function [B]=vec_b(a,b,n,fun)

h=(b-a)/n;
x=[a:h:b];

B=zeros(n+1,1);

for k=1:n
    B(k)=B(k)+simpson(@profphi,x(k),x(k+1),fun,2,x(k),x(k+1));
    B(k+1)=B(k+1)+simpson(@profphi,x(k),x(k+1),fun,1,x(k),x(k+1));
end


