x=0:1/100:1;
y1=phi3(x,1,0,1);
y2=phi3(x,2,0,1);
y3=phi3(x,3,0,1);
y4=phi3(x,4,0,1);

%plot(x,y1,x,y3,x,y2,x,y4)

type=0;a=0;b=1;n=30;
upa=0;upb=0;
ua=1;ub=-1;
alpha=1;beta=1;

M=mat_masse3(a,b,n);
R=mat_rigidite3(a,b,n);
B=vec_b3(a,b,n,@f3);

A= alpha*M + beta*R;

% Neumann
if (type==0)
    A(1,1) = 10^7; A(2*n+2,2*n+2) = 10^7;
    B(1) = B(1)-upa; B(2*n+2) = B(2*n+2)+upb;
end

c=A\B;

% Dirichlet
if(type==1)
    Anew = A([2:n],[2:n]);
    Bnew = B - ua*A(:,1) - ub*A(:,n+1);
    Bnew = Bnew([2:n]);
    
    % Calcul de la solution
    c(1) = ua;
    c([2:n]) = Anew\Bnew;
    c(n+1) = ub;
end


x = linspace(a, b, 2*n+2);


plot(x,c,'red')
axis equal