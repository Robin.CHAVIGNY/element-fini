function [y]=phi3prim(x,i,a,b)

if (i==3 || i==4)
    tmp=a;
    a=b;
    b=tmp;
    i=i-2;
end

if (i==1)
    y=6*(x-b)*(x-a)/((b-a)^3);
else
    y=(x-b)*(3*x-2*a-b)/((b-a)^2);
end
