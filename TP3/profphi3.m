function [y]=profphi3(x,f,i,a,b)
phi=phi3(x,i,a,b);
y0=feval(f,x);
y=y0*phi;