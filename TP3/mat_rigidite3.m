function [R]=mat_rigidite3(a,b,n)

R=sparse(2*n+2,2*n+2);

h=(b-a)/n;
x=[a:h:b];

for k = 1:n % Considération du k-ieme intervalle [xm(k),xm(k+1)] :
    for il =1:4 % il = ilocal
        ig = 2*(k-1) + il;% ig = iglobal
        for jl = 1:4 % jl = jlocal
            jg = 2*(k-1) + jl; % jg = jglobal
            R(ig,jg) = R(ig,jg) + simpson(@proprimphi3,x(k),x(k+1),x(k),x(k+1),il,jl);
        end
    end
end