function [y]=phi3(x,i,a,b)

if (i==3 || i==4)
    tmp=a;
    a=b;
    b=tmp;
    i=i-2;
end 

if (i==1)
    y=(x-b).^2 .* (2.*(x-a)./(b-a) +1) ./ (a-b)^2;
elseif(i==2)
    y=(x-b).^2 .* (x-a)./(a-b).^2;
end

