function [y]=proprimphi3(x,a,b,iloc,jloc)


phii=phi3prim(x,iloc,a,b);
phij=phi3prim(x,jloc,a,b);

y=phii*phij;