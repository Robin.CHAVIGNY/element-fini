function [y]=prophi3(x,a,b,i1,i2)

phii=phi3(x,i1,a,b);
phij=phi3(x,i2,a,b);

y=phii*phij;