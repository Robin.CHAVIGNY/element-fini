function [B]=vec_b3(a,b,n,fun)

h=(b-a)/n;
x=[a:h:b];

B=zeros(2*n+2,1);

for k=1:n
    for il=1:4
        ig=2*(k-1)+il;
        B(ig)=B(ig)+simpson(@profphi3,x(k),x(k+1),fun,il,x(k),x(k+1));
    end
end
