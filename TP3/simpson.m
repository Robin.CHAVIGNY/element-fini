function [y]=simpson(f,a,b,varargin)

m=(a+b)/2;

fa=feval(f,a,varargin{:});
fm=feval(f,m,varargin{:});
fb=feval(f,b,varargin{:});

y=(b-a)*(fa+4*fm+fb)/6;